# adamlink demo website 

Super simple slim setup as AdamLink demo, with just json output, no templates and EasyRDF

Run local dev server:

    php -S 0.0.0.0:8888 -t public public/index.php
        

## Todo

    * filter /search per collection
    * paginate
    * show OPTIONAL(dates), 
    * order by dates or something else
    * make it also work for buildings
     

##Tools 
* http://www.runningcoder.org/jquerytypeahead/demo/
* http://ashleydw.github.io/lightbox/