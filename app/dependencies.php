<?php
// DIC configuration

use App\Sparql\AdamLinkSparql;

$container = $app->getContainer();

$container['sparqler'] = function ($c) {
    return new AdamLinkSparql();
};
