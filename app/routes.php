<?php
// Routes

use App\Sparql\AdamLinkSparql;
use Slim\Http\Request;
use Slim\Http\Response;

/** @var $app \Slim\App */

$app->get('/', function (Request $request, Response $response) {
    $template = $this->get('settings')['view']['template_path'] . '/' . 'home.phtml';
    $response->getBody()->write(file_get_contents($template));
    return $response;
})->setName('home');


$app->get('/straten', function (Request $request, Response $response) {
    $template = $this->get('settings')['view']['template_path'] . '/' . 'street.phtml';
    $response->getBody()->write(file_get_contents($template));
    return $response;
})->setName('street');


$app->get('/iisg', function (Request $request, Response $response) {

})->setName('iisg');


$app->get('/sparql/street', function (Request $request, Response $response, $args)  {
    $uri = $request->getParam('uri');

    $start = $request->getParam('start') ?? 0;
    $limit = 100;

    /** @var AdamLinkSparql $sparqler */
    $sparqler = $this->sparqler;

    $hits = $sparqler->countImagesForUri($uri);
    $json = [
        'hits' => $hits,
        'records' => $sparqler->getImagesForUri($uri, $start)
    ];

    // todo only gets a 100 images now... should really paginate
    return $response->withJson($json);
})->setName('sparql-street');

