<?php declare(strict_types=1);

namespace App\Sparql;

use EasyRdf_Sparql_Client;

class IISGSparql {

    private $endpoint = 'https://api.adamnet.triply.cc/datasets/Petra/saa/containers/lod/sparql';
    private $client;

    public function __construct()
    {
        $this->client = new EasyRdf_Sparql_Client($this->endpoint);
    }

    public function getRecordsForKraakpanWithName(string $name) : array
    {
        $result = $this->client->query(
            "SELECT ?item, ?name, ?orgName, ?pid WHERE {
                ?item <http://schema.org/about> ?org .
                ?org <https://iisg.amsterdam/marc/110-2--a> ?orgName .
                ?orgName bif:contains \"'{$name}*'\".
                ?item ns1:name ?name .
                ?item <https://iisg.amsterdam/marc/655--4-a> ?genre .
                ?item <https://iisg.amsterdam/marc/852-4--p> ?pid .
                FILTER (?genre =\"Photo.\"^^xsd:string)
            }"
        );

        $output = [];
        foreach ($result as $row) {
            $output[] = [
                'item' => (string) $row->item,
                'name' => (string) $row->name,
                'image' => (string) $row->image,
                'thumb' => str_replace('640x480', '100x100', (string) $row->image),
                'orgName' => (string) $row->orgName
            ];
        }
        return $output;
    }

    private function mapPidToImageUri(string $pid)
    {
        // get pid en plak hierin:
        //https://search.socialhistory.org/Cover/Show?size=small&pid=30051001306247&publication=pictoright
    }

    private function toJson(\EasyRdf_Sparql_Result $result)
    {
        $fields = $result->getFields();
        $output = [];
        foreach ($result as $key => $row) {
            $output[] = [
                $fields[$key]
            ];
            echo $row->title . " -- " .$row->image . "<br>";
        }
    }
}