<?php declare(strict_types=1);

namespace App\Sparql;

use EasyRdf_Sparql_Client;

class AdamLinkSparql {

    private $endpoint = 'https://api.druid.datalegend.net/datasets/adamnet/all/services/endpoint/sparql';

    private $client;

    public function __construct()
    {
        $this->client = new EasyRdf_Sparql_Client($this->endpoint);
    }

    public function getDatasetsWithImages()
    {
        $result = $this->client->query(
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT DISTINCT(?void) WHERE {
              ?sub void:inDataset ?void .
              ?sub foaf:depiction ?image .
              ?sub dct:spatial ?ding
            }"
        );
    }

    public function countImagesForUri(string $uri) : int
    {
        $result = $this->client->query(
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT COUNT(?sub) as ?hits WHERE {
              ?sub dc:type ?type .
              ?sub dc:title ?title .
              ?sub dc:description ?description .
              ?sub foaf:depiction ?image .
              ?sub dct:spatial <{$uri}> 
            } "
        );

        $hits = (string) $result->current()->hits;
        return (int) $hits;
    }

    public function getImagesForUri(string $uri, int $start, int $limit = 100) : array
    {
        $result = $this->client->query(
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT ?title ?image ?sub ?description WHERE {
              ?sub dc:type ?type .
              ?sub dc:title ?title .
              ?sub dc:description ?description .
              ?sub foaf:depiction ?image .
              ?sub dct:spatial <{$uri}> 
            }
            LIMIT {$limit} OFFSET {$start}
            "
        );

        $output = [];
        foreach ($result as $row) {
            $output[] = [
                'title' => (string) $row->title,
                'description' => (string) $row->description,
                'image' => (string) $row->image,
                'thumb' => str_replace('640x480', '200x200', (string) $row->image),
                'subject' => (string) $row->sub
            ];
        }
        return $output;
    }

    private function toJson(\EasyRdf_Sparql_Result $result)
    {
        $fields = $result->getFields();
        $output = [];
        foreach ($result as $key => $row) {
            $output[] = [
                $fields[$key]
            ];
            echo $row->title . " -- " .$row->image . "<br>";
        }
    }
}