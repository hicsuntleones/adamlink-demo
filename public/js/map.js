// map stuff

$(document).ready(function () {

    var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    });

    var cartodb = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
    });

    // init map first
    map = L.map('map', {
        center: [52.372106, 4.90044], // niewmarkt
        zoom: 13,
    });

    map.addLayer(cartodb);

    var adamlinkStyle = {
        color: "#FC0011",
        weight: 5,
        opacity: 1,
        fillOpacity: 0.5
    };

    // simple assignment: L.geoJSON(geojsonFeature).addTo(map);

    // create a layer so we can add data later
    streetLayer = L.geoJSON().addTo(map);


});