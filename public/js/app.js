// this here app thingies

$(document).ready(function () {

});

// enable data-includes
$(function(){
    var includes = $('[data-include]');
    jQuery.each(includes, function(){
        var file = 'partials/' + $(this).data('include') + '.html';
        $(this).load(file);
    });
});

// enable tooltips
$("body").tooltip({
    selector: '[data-toggle="tooltip"]'
});

// set visibility
// (function($) {
//     $.fn.invisible = function() {
//         return this.each(function() {
//             $(this).css("visibility", "hidden");
//         });
//     };
//     $.fn.visible = function() {
//         return this.each(function() {
//             $(this).css("visibility", "visible");
//         });
//     };
// }(jQuery));