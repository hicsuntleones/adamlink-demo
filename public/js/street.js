// search streets
$.typeahead({
    input: ".js-typeahead",
    minLength: 2,
    maxItem: 15,
    order: "asc",
    hint: true,
    accent: true,
    searchOnFocus: true,
    display: ["prefLabel"],
    template: '<span data-series="{{prefLabel}}">' +
    '{{id}} - {{prefLabel}}' +
    '</span>',
    templateValue: "{{prefLabel}}",
    emptyTemplate: "Geen straat gevonden voor {{query}}",
    source: {
        streets: {
            ajax: {
                url: '/data/streets.json',
                data: {
                    q: '{{query}}'
                },
                //path: 'response.data.user'
            }
        }
    },
    callback: {
        onClick: function (node, a, street, event) {
            //alert(JSON.stringify(street));

            var resourceUri = encodeURIComponent(street.uri);
            loadImages('/sparql/street?uri=' + resourceUri, '#streetPics');
            addGeometryToMap(street);
        }
    },
    debug: true,
});


function loadImages(url, container) {
    clearImages(container);

    $.getJSON(url, function (json) {
        var hits = json.hits;
        var imgList = '';
        $.each(json.records, function () {
            imgList += createCard(this);
        });
        // of beter: addTo ImageCarousel or lightbox or whatever het wordt
        if (imgList.length > 2) {
            $('#message').html('<p class="text-success">Totaal aantal afbeeldingen: ' + hits + '. '+ showNr(hits)+'</p>');
            $(container).append(imgList);
        } else {
            $('#message').html('<p class="text-danger">Sorry, geen foto\'s gevonden voor die straat.');
        }

    });
};

function showNr(hits) {
    if (hits > 99) {
        return 'Getoond: de eerste 100';
    }
    return '';
}

function createCard(item) {
    var html =
        '<div class="" style="height: 160px; float:left">' +
        '<a href="' + item.image + '" data-title="' + item.title + '" data-footer="'+ item.description.replace(/["']/g, "") +
        '<br/><a href=\''+ item.subject + '\' target=\'_blank\'>Bekijk collectie-item</a>" ' +
        'data-toggle="lightbox" data-gallery="street">' +
        '<img width="200" class="img-thumbnail" src="' + item.thumb + '" alt="foto" ' +
        'data-toggle="tooltip" data-placement="top" title="' + item.title + '"></a>' +
        '</div>';
    return html;
}

function initPagination(hits) {
    $('#pagination-demo').twbsPagination({
        totalPages: hits,
        visiblePages: 4,
        onPageClick: function (event, page) {
            $('#page-content').text('Page ' + page);
        }
    });
}

function addGeometryToMap(street) {
    //var resourceUri = encodeURIComponent(street.uri);

    // show uri below map
    $('#uri').html('<h3>' + street.prefLabel + '</h3>' +
        '<a href="'+ street.uri + '" title="Bekijk details van de straat op Adamlink">'+ street.uri + '</a>' +
        '');

    var streetUri = street.uri + '.geojson';

    // get the geoJson feature directly from adamlink
    $.getJSON(streetUri, function (geojsonFeature) {
        streetLayer.clearLayers();
        streetLayer.addData(geojsonFeature);
        map.fitBounds(streetLayer.getBounds());
    });

}

function clearImages(container) {
    $(container).empty();
}

